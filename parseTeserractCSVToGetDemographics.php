<?php
/* 
this script is used to read the resuts from the ocr tasks which is in the form of csv files.
It will take the directory path as the input and then perform the parsing to extract the OCR results into the demographics table 
*/
require_once('lib/LIB_parse.php');
require_once('lib/LIB_http.php');
require_once('loginDetails.php');
$conn=$conn3;
$directoryPath=$argv[1];

$processedFiles=array_diff(scandir($directoryPath), array('.', '..'));

$insertQuery = "INSERT INTO `demographics`(`city`,`accode`, `pollingboothId`, `gender`, `age`,`filename`) VALUES  ";

foreach($processedFiles as $fileName)
{
    $city="Bangalore";
    //$fileName=$argv[1];   
    $insertOutletsValues = array();
    $filePath=$directoryPath.$fileName;

    $file=fopen($filePath,"r");
    echo "parsing..".$fileName."...";
    list($str,$extension)=explode('.',$fileName);
    $acCode=substr($str,4,3);
    $pollingBoothId=substr($str,8);
    echo $acCode.",".$pollingBoothId."\n";
    //read the csv file 
    while(! feof($file))
    {

        $fstring=fgets($file);
        #echo $fstring."################";
        if (stripos($fstring, 'Age:') !== false) {
            $ages=parse_array($fstring,"Age:","ALE");
            #$ages=explode("Age",$fstring);
            #$genders=explode("")
            print_r($ages);
            foreach($ages as $data)
            {
                $age=return_between($data,"Age","Gender",EXCL);
                $age=str_ireplace(":",'',$age);
                $age=intval(trim($age));
                echo $age."\n";

                $gender=return_between($data,"Gender","ALE",EXCL);
                $gender=str_ireplace(":",'',$gender);
                $gender=trim($gender);
                if($gender[0]=='M')
                    echo "Male \n";
                else if($gender[0]=='F')
                    echo "Female \n";
                
                $insertOutletsValues[] = "('".$city."','".$acCode."',
                '".$pollingBoothId."',
                '".$gender[0]."',
                '".$age."','".$fileName."')";
                
            }
        }
        else
            continue;
        

    } 
    
    $finalInsertQuery = $insertQuery . implode(' , ' , $insertOutletsValues);
     //echo $insertGrid."\n";
     if ($conn->query($finalInsertQuery) === FALSE) {
        echo "Error: " . $finalInsertQuery . "<br>" . $conn->error;
        $insertOutletsValues = array();
    }
    //$insertOutletsValues = array();

}







?>