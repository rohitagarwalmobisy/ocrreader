import os
#from matplotlib import pyplot
from PIL import Image
import numpy as np
import pytesseract
from pdf2image import convert_from_path
import glob
import pandas as pd
import time
import sys

def pdf_to_images(pdf_filename):
    image_folder = pdf_filename[:-4]
    try:
        os.mkdir(image_folder)
    except :
        print('mkdir failed!')
        return "1"
    pages = convert_from_path(pdf_filename,
                              dpi=500,
                              output_folder=image_folder,
                              output_file=os.path.basename(image_folder),
                              fmt='jpg',
                              grayscale= True)
    image_list = glob.glob(image_folder + '/*.jpg')
    return image_list


def image_to_crop(image_path):
    img = Image.open(image_path)
    crop_folder_path = image_path[:-4]
    try:
        os.mkdir(crop_folder_path)
    except :
        print('mkdir failed!')
    w, h = 1255, 490
    k = 0
    for i in range(3):
        for j in range(10):
            k = k + 1
            x1 = 160 + (w*i)
            x2 = x1 + w
            y1 = 280 + (h*j)
            y2 = y1 + h
            img_crop = img.crop(box=(x1, y1, x2, y2))
            img_crop.save(os.path.join(crop_folder_path, os.path.basename(image_path)[:-4] + '_' + str(k) + image_path[-4:]))

def get_text(img):
    #this config need to be set to config=r'-l hin' in case of hindi language . ensure that 
    #the language file ../languages/hin.traineddata has been also copied to usr/share/tesseract-ocr/4.00/tessdata/
    #config=r'-l hin'
    #fro english None is OK
    config = None
    return pytesseract.image_to_string(img, config=config)




def pdf_to_df(pdf_filename):
    image_list = pdf_to_images(pdf_filename)
    if(image_list=="1"):
        return
#     for image_path in image_list:
#         image_to_crop(image_path)
    
    text_list = []
    for k,image_path in enumerate(image_list):
        print(k)
        text_list.append([get_text(image_path),
                              os.path.basename(image_path)])
#         crop_img_list = glob.glob(image_path[:-4] + '/*.jpg')
#         for crop_img in crop_img_list:
#             text_list.append([get_text(crop_img),
#                               os.path.basename(image_path)])
    df = pd.DataFrame(data=text_list)
    df.to_csv(pdf_filename[:-4] +'.csv', index=False)


#the set of folder naes needs to be changed based on which particular files to be parsed
folders=['mumbai/ac180/*.PDF','mumbai/ac179/*.PDF']
for folder in folders:
    print("checknf ",folder)
    for pdf_filename in glob.glob(folder):
        t1 = time.time()
        if(os.path.exists(os.path.splitext(pdf_filename)[0]+".csv")):
            print(os.path.splitext(pdf_filename)[0]+".csv","already exists so skipping")
            continue
        else:
            print("working on ",pdf_filename)
            pdf_to_df(pdf_filename)
            t2 = time.time() - t1
            print(t2)
