import wget
import time
import random

#assembly list
ac_list=list(range(150,180))

#polling station list
p_list = list(range(295, 530))

#start the dump. Each file will look like S10A<assembly>P<polling>.pdf
k = 0
for x in ac_list:
    for y in p_list:
        k = k + 1
        try:
            data = 'http://ceo.karnataka.gov.in/finalrolls_2020/English/MR/AC' + str(x) + '/S10A' + str(x) + 'P' + str(y) + '.pdf'
            wget.download(data)
            #random sleep to simulate a real user
            time.sleep(random.randint(1, 5))
        except :
            continue